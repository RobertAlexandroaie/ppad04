package dc.ac.ppad.core.utils;

public class ConnectionConstants {

	private ConnectionConstants() {
		
	}
	
	public static int PORT = 6666;
	public static String LOCALHOST = "127.0.0.1";
}
