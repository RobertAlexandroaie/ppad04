package dc.ac.ppad.main;

import static dc.ac.ppad.core.utils.ConnectionConstants.PORT;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class DatagramServer {
	
	private DatagramSocket socket = null;
	private DatagramPacket request, response = null;

	public DatagramServer() throws IOException {
		
		socket = new DatagramSocket(PORT);
	}

	public void startServer() throws IOException {
	
		try {
		
			while (true) {
			
				byte[] contentBuffer = null;
				
				// Crearea pachetului in care va fi receptionata cererea
				contentBuffer = new byte[256];
				int contentBufferLength = contentBuffer.length;
				request = new DatagramPacket(contentBuffer, contentBufferLength);
				
				// Preluarea pachetului cu cererea
				socket.receive(request);
				
				// Aflarea adresei si a portului de la care vine cererea
				InetAddress address = request.getAddress();
				int port = request.getPort();
				System.out.println("Adresa si port client: " + address + ":" + port);
				
				// Construirea raspunsului
				String responseContent = "Hello " + new String(request.getData(), request.getOffset(), request.getLength());
				contentBuffer = responseContent.getBytes();
				
				// Trimite un pachet cu raspunsul catre client
				response = new DatagramPacket(contentBuffer, contentBufferLength, address, port);
				socket.send(response);
			}
		} finally {
			socket.close();
		}
	}

	public static void main(String[] args) throws IOException {
		DatagramServer ds = new DatagramServer();
		ds.startServer();
	}
}